package org.apache.shindig.gadgets.parse.nekohtml;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shindig.gadgets.parse.GadgetHtmlParser;
import org.apache.shindig.gadgets.parse.HtmlSerializer;
import org.apache.shindig.gadgets.parse.ParseModule;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;

/**
 * Test behavior of simplified HTML parser
 */
public class NekoParsersTest {

  /** The vm line separator */
  private static final String EOL = System.getProperty( "line.separator" );

  private NekoSimplifiedHtmlParser simple = new NekoSimplifiedHtmlParser(
        new ParseModule.DOMImplementationProvider().get());
  private NekoHtmlParser full = new NekoHtmlParser(
        new ParseModule.DOMImplementationProvider().get());

  @Test
  public void testDocWithDoctype() throws Exception {
    // Note that doctype is properly retained
    String content = readFile("org/apache/shindig/gadgets/parse/nekohtml/test.html");
    String expected = readFile("org/apache/shindig/gadgets/parse/nekohtml/test-expected.html");
    //parseAndCompareBalanced(content, expected, full);
    parseAndCompareBalanced(content, expected, simple);
  }

  @Test
  public void testDocNoDoctype() throws Exception {
    // Note that no doctype is properly created when none specified
    String content = readFile("org/apache/shindig/gadgets/parse/nekohtml/test-fulldocnodoctype.html");
    assertNull(full.parseDom(content).getDoctype());
    assertNull(simple.parseDom(content).getDoctype());
  }

  @Test
  public void testNotADocument() throws Exception {
    // Note that no doctype is injected for fragments
    String content = readFile("org/apache/shindig/gadgets/parse/nekohtml/test-fragment.html");
    String expected = readFile("org/apache/shindig/gadgets/parse/nekohtml/test-fragment-expected.html");
    parseAndCompareBalanced(content, expected, full);
    parseAndCompareBalanced(content, expected, simple);
  }

  @Test
  public void testNoBody() throws Exception {
    // Note that no doctype is injected for fragments
    String content = readFile("org/apache/shindig/gadgets/parse/nekohtml/test-headnobody.html");
    String expected = readFile("org/apache/shindig/gadgets/parse/nekohtml/test-headnobody-expected.html");
    parseAndCompareBalanced(content, expected, full);
    parseAndCompareBalanced(content, expected, simple);
  }

  private void parseAndCompareBalanced(String content, String expected, GadgetHtmlParser parser)
      throws Exception {
    Document document = parser.parseDom(content);
    expected = StringUtils.replace(expected, EOL, "\n");
    assertEquals(expected, HtmlSerializer.serialize(document));
  }

  private String readFile(final String relativePath) throws IOException {
    final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(relativePath);
    assertNotNull("No such file '" + relativePath + "'", inputStream);
    return IOUtils.toString(inputStream, UTF_8);
  }
}