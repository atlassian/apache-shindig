/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.gadgets.preload;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.shindig.gadgets.GadgetContext;
import org.apache.shindig.gadgets.GadgetException;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.junit.Test;

import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class DefaultPreloaderServiceTest {
  private static final String PRELOAD_STRING_KEY = "key a";
  private static final String PRELOAD_NUMERIC_KEY = "key b";
  private static final String PRELOAD_MAP_KEY = "key c";
  private static final String PRELOAD_STRING_VALUE = "Some random string";
  private static final Integer PRELOAD_NUMERIC_VALUE = 5;
  private static final Map<String, String> PRELOAD_MAP_VALUE = ImmutableMap.of("foo", "bar", "baz", "blah");

  private final TestPreloader preloader = new TestPreloader();
  private final TestPreloader preloader2 = new TestPreloader();

  @Test
  public void preloadSingleService() throws GadgetException {
    preloader.tasks.put(PRELOAD_STRING_KEY, new DataPreload(PRELOAD_STRING_VALUE));

    PreloaderService service = new DefaultPreloaderService(singletonList(preloader));

    assertEquals(PRELOAD_STRING_VALUE, service.preload(null, null).getData(PRELOAD_STRING_KEY).toJson());
  }

  @Test
  public void preloadMultipleServices() throws GadgetException {
    preloader.tasks.put(PRELOAD_STRING_KEY, new DataPreload(PRELOAD_STRING_VALUE));
    preloader.tasks.put(PRELOAD_NUMERIC_KEY, new DataPreload(PRELOAD_NUMERIC_VALUE));
    preloader2.tasks.put(PRELOAD_MAP_KEY, new DataPreload(PRELOAD_MAP_VALUE));
    PreloaderService service = new DefaultPreloaderService(asList(preloader, preloader2));

    Preloads preloads = service.preload(null, null);

    assertEquals(PRELOAD_STRING_VALUE, preloads.getData(PRELOAD_STRING_KEY).toJson());
    assertEquals(PRELOAD_NUMERIC_VALUE, preloads.getData(PRELOAD_NUMERIC_KEY).toJson());
    assertEquals(PRELOAD_MAP_VALUE, preloads.getData(PRELOAD_MAP_KEY).toJson());
  }

  @Test(expected = GadgetException.class)
  public void exceptionsArePropagated() throws GadgetException {
    Preloader exceptionThrowingPreloader = (context, gadget) -> {
      throw new GadgetException(GadgetException.Code.BLACKLISTED_GADGET);
    };
    PreloaderService service = new DefaultPreloaderService(singletonList(exceptionThrowingPreloader));

    service.preload(null, null);
  }

  private static class TestPreloader implements Preloader {
    private final Map<String, PreloadedData> tasks = Maps.newHashMap();

    public Map<String, PreloadedData> createPreloadDataMap(GadgetContext context, GadgetSpec spec) {
      return tasks;
    }
  }

  private static class DataPreload implements PreloadedData {
    private final Object data;

    public DataPreload(Object data) {
      this.data = data;
    }

    public Object toJson() {
      return data;
    }
  }
}
