/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.gadgets.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.shindig.auth.AnonymousSecurityToken;
import org.apache.shindig.auth.AuthInfo;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.gadgets.GadgetContext;
import org.junit.Test;

import java.util.Locale;

public class HttpGadgetContextTest extends ServletTestFixture {

  @Test
  public void testIgnoreCacheParam() {
    when(request.getParameter("nocache")).thenReturn(Integer.toString(Integer.MAX_VALUE));
    GadgetContext context = new HttpGadgetContext(request);
    assertTrue(context.getIgnoreCache());
  }

  @Test
  public void testLocale() {
    when(request.getParameter("lang")).thenReturn(Locale.CHINA.getLanguage());
    when(request.getParameter("country")).thenReturn(Locale.CHINA.getCountry());
    GadgetContext context = new HttpGadgetContext(request);
    assertEquals(Locale.CHINA, context.getLocale());
  }

  @Test
  public void testDebug() {
    when(request.getParameter("debug")).thenReturn("1");
    GadgetContext context = new HttpGadgetContext(request);
    assertTrue(context.getDebug());
  }

  @Test
  public void testGetParameter() {
    when(request.getParameter("foo")).thenReturn("bar");
    GadgetContext context = new HttpGadgetContext(request);
    assertEquals("bar", context.getParameter("foo"));
  }

  @Test
  public void testGetHost() {
    when(request.getHeader("Host")).thenReturn("foo.org");
    GadgetContext context = new HttpGadgetContext(request);
    assertEquals("foo.org", context.getHost());
  }

  @Test
  public void testGetUserIp() {
    when(request.getRemoteAddr()).thenReturn("2.3.4.5");
    GadgetContext context = new HttpGadgetContext(request);
    assertEquals("2.3.4.5", context.getUserIp());
  }

  @Test
  public void testGetSecurityToken() {
    SecurityToken expected = new AnonymousSecurityToken();
    when(request.getAttribute(AuthInfo.Attribute.SECURITY_TOKEN.getId())).thenReturn(expected);
    GadgetContext context = new HttpGadgetContext(request);
    assertEquals(expected, context.getToken());
  }

  @Test
  public void testGetHostWhenNoHostHeaderPresent() {
    when(request.getRequestURL()).thenReturn(new StringBuffer("http://example.com/gadgets/123"));
    GadgetContext context = new HttpGadgetContext(request);
    assertEquals("example.com", context.getHost());
  }
}