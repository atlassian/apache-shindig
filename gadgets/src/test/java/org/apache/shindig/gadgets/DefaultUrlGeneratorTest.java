package org.apache.shindig.gadgets;

import com.google.common.collect.ImmutableList;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.shindig.common.ContainerConfig;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.net.URI;
import java.util.List;
import java.util.Locale;

import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static org.apache.shindig.gadgets.DefaultUrlGenerator.IFRAME_URI_PARAM;
import static org.apache.shindig.gadgets.DefaultUrlGenerator.JS_URI_PARAM;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultUrlGeneratorTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    private static final String CONTAINER = "shindig";
    private static final String IFR_BASE = "/gadgets/eye-frame";
    private static final String JS_BASE = "http://%host%/get-together/livescript/%js%";
    private static final String MODULE_ID = "3435";
    private static final String SPEC_URL = "http://example.org/gadget.xml";
    private static final String TYPE_URL_HREF_HOST = "opensocial.org";
    private static final String TYPE_URL_HREF_PATH = "/app/foo";
    private static final String TYPE_URL_HREF_QUERY = "bar=baz&foo=bar";
    private static final String TYPE_URL_HREF = "http://" + TYPE_URL_HREF_HOST + TYPE_URL_HREF_PATH + '?' + TYPE_URL_HREF_QUERY;
    private static final String UP_NAME = "user-pref-name";
    private static final String UP_VALUE = "user-pref-value";
    private static final String VIEW = "canvas";

    @Mock
    private ContainerConfig config;

    @Mock
    private GadgetContext context;

    @Mock
    private GadgetFeatureRegistry registry;

    @Mock
    private LockedDomainService lockedDomainService;

    private DefaultUrlGenerator defaultUrlGenerator;

    @Before
    public void setUp() throws Exception {
        final UserPrefs prefs = mock(UserPrefs.class);
        when(prefs.getPref(UP_NAME)).thenReturn(UP_VALUE);

        when(context.getContainer()).thenReturn(CONTAINER);
        when(context.getLocale()).thenReturn(Locale.getDefault());
        when(context.getModuleId()).thenReturn(MODULE_ID);
        when(context.getUrl()).thenReturn(URI.create(SPEC_URL));
        when(context.getUserPrefs()).thenReturn(prefs);
        when(context.getView()).thenReturn(VIEW);

        when(config.getContainers()).thenReturn(singleton(CONTAINER));
        when(config.get(CONTAINER, IFRAME_URI_PARAM)).thenReturn(IFR_BASE);
        when(config.get(CONTAINER, JS_URI_PARAM)).thenReturn(JS_BASE);

        defaultUrlGenerator = new DefaultUrlGenerator(config, lockedDomainService, registry);
    }

    @Test
    public void testGetBundledJsParam() {
        final List<String> features = ImmutableList.of("foo", "bar");
        when(context.getDebug()).thenReturn(true);

        final String jsParam = defaultUrlGenerator.getBundledJsParam(features, context);

        assertTrue(jsParam.matches("foo:bar\\.js\\?v=[0-9a-zA-Z]*&container=" + CONTAINER + "&debug=1"));
    }

    @Test
    public void testGetBundledJsParamWithBadFeatureName() {
        final List<String> features = ImmutableList.of("foo!", "bar");
        when(context.getDebug()).thenReturn(true);

        final String jsParam = defaultUrlGenerator.getBundledJsParam(features, context);

        assertTrue(jsParam.matches("bar\\.js\\?v=[0-9a-zA-Z]*&container=" + CONTAINER + "&debug=1"));
    }

    @Test
    public void testGetBundledJsParamWithNoFeatures() {
        final List<String> features = emptyList();
        when(context.getDebug()).thenReturn(false);

        final String jsParam = defaultUrlGenerator.getBundledJsParam(features, context);

        assertTrue(jsParam.matches("core\\.js\\?v=[0-9a-zA-Z]*&container=" + CONTAINER + "&debug=0"));
    }

    @Test
    public void testGetBundledJsUrl() {
        final List<String> features = ImmutableList.of("foo", "bar");
        when(context.getDebug()).thenReturn(false);
        when(context.getHost()).thenReturn("example.org");

        final String jsParam = defaultUrlGenerator.getBundledJsUrl(features, context);

        final Uri uri = Uri.parse(jsParam);

        assertEquals("example.org", uri.getAuthority());
        assertEquals("/get-together/livescript/foo:bar.js", uri.getPath());
        final String v = uri.getQueryParameter("v");
        assertNotNull(v);
        assertTrue("Missing checksum", v.matches("[0-9a-zA-Z]+"));
        assertEquals(CONTAINER, uri.getQueryParameter("container"));
        assertEquals("0", uri.getQueryParameter("debug"));
    }

    @Test
    public void testGetIframeUrlTypeHtml() throws Exception {
        final String xml
                = "<Module>" +
                " <ModulePrefs title='test'/>" +
                " <Content type='html'/>" +
                " <UserPref name='" + UP_NAME + "' datatype='string'/>" +
                "</Module>";
        final GadgetSpec spec = new GadgetSpec(Uri.parse(SPEC_URL), xml);

        final Gadget gadget = new Gadget()
                .setContext(context)
                .setSpec(spec)
                .setCurrentView(spec.getView("default"));

        final Uri iframeUrl = Uri.parse(defaultUrlGenerator.getIframeUrl(gadget));

        assertEquals(IFR_BASE, iframeUrl.getPath());
        assertEquals(CONTAINER, iframeUrl.getQueryParameter("container"));
        assertEquals(UP_VALUE, iframeUrl.getQueryParameter("up_" + UP_NAME));
        assertEquals(MODULE_ID, iframeUrl.getQueryParameter("mid"));
        assertEquals(VIEW, iframeUrl.getQueryParameter("view"));
    }

    @Test
    public void testGetIframeUrlTypeHtmlWithLockedDomain() throws Exception {
        final String xml
                = "<Module>" +
                " <ModulePrefs title='test'/>" +
                " <Content type='html'/>" +
                " <UserPref name='" + UP_NAME + "' datatype='string'/>" +
                "</Module>";
        final GadgetSpec spec = new GadgetSpec(Uri.parse(SPEC_URL), xml);

        when(lockedDomainService.getLockedDomainForGadget(any(GadgetSpec.class), eq(CONTAINER)))
                .thenReturn("locked.example.org");

        final Gadget gadget = new Gadget()
                .setContext(context)
                .setSpec(spec)
                .setCurrentView(spec.getView("default"));

        final Uri iframeUrl = Uri.parse(defaultUrlGenerator.getIframeUrl(gadget));

        assertEquals("locked.example.org", iframeUrl.getAuthority());
        assertEquals(IFR_BASE, iframeUrl.getPath());
        assertEquals(CONTAINER, iframeUrl.getQueryParameter("container"));
        assertEquals(UP_VALUE, iframeUrl.getQueryParameter("up_" + UP_NAME));
        assertEquals(MODULE_ID, iframeUrl.getQueryParameter("mid"));
        assertEquals(VIEW, iframeUrl.getQueryParameter("view"));
    }

    @Test
    public void testGetIframeUrlTypeUrl() throws Exception {
        final String xml
                = "<Module>" +
                " <ModulePrefs title='test'/>" +
                " <Content type='url' href='" + StringEscapeUtils.escapeHtml4(TYPE_URL_HREF) + "'/>" +
                " <UserPref name='" + UP_NAME + "' datatype='string'/>" +
                "</Module>";
        final GadgetSpec spec = new GadgetSpec(Uri.parse(SPEC_URL), xml);

        final Gadget gadget = new Gadget()
                .setContext(context)
                .setSpec(spec)
                .setCurrentView(spec.getView("default"));

        final URI iframeUrl = URI.create(defaultUrlGenerator.getIframeUrl(gadget));

        assertEquals(TYPE_URL_HREF_HOST, iframeUrl.getAuthority());
        assertEquals(TYPE_URL_HREF_PATH, iframeUrl.getPath());
        assertThat(iframeUrl.getQuery(), containsString(TYPE_URL_HREF_QUERY));
        assertThat(iframeUrl.getQuery(), containsString("container=" + CONTAINER));
        assertThat(iframeUrl.getQuery(), containsString("up_" + UP_NAME + '=' + UP_VALUE));
        assertThat(iframeUrl.getQuery(), containsString("mid=" + MODULE_ID));
    }
}
