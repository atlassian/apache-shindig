/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets;

import org.apache.shindig.common.ContainerConfig;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static java.util.Collections.singletonList;
import static org.apache.shindig.common.ContainerConfig.DEFAULT_CONTAINER;
import static org.apache.shindig.gadgets.HashLockedDomainService.LOCKED_DOMAIN_REQUIRED_KEY;
import static org.apache.shindig.gadgets.HashLockedDomainService.LOCKED_DOMAIN_SUFFIX_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HashLockedDomainServiceTest {
  private HashLockedDomainService lockedDomainService;
  private final GadgetSpec wantsLocked = makeSpec(true, "http://somehost.com/somegadget.xml");
  private final GadgetSpec notLocked = makeSpec(false, "not-locked");
  private final ContainerConfig requiredConfig = mock(ContainerConfig.class);
  private final ContainerConfig enabledConfig = mock(ContainerConfig.class);

  private static GadgetSpec makeSpec(boolean wantsLocked, String url) {
    String gadgetXml;
    if (wantsLocked) {
      gadgetXml =
          "<Module><ModulePrefs title=''>" +
          "  <Require feature='locked-domain'/>" +
          "</ModulePrefs><Content/></Module>";
    } else {
      gadgetXml = "<Module><ModulePrefs title=''/><Content/></Module>";
    }

    try {
      return new GadgetSpec(Uri.parse(url), gadgetXml);
    } catch (GadgetException e) {
      return null;
    }
  }

  @Before
  public void setUp() throws Exception {
    when(requiredConfig.get(DEFAULT_CONTAINER, LOCKED_DOMAIN_SUFFIX_KEY)).thenReturn("-a.example.com:8080");
    when(requiredConfig.get(DEFAULT_CONTAINER, LOCKED_DOMAIN_REQUIRED_KEY)).thenReturn("true");
    when(requiredConfig.getContainers()).thenReturn(singletonList(DEFAULT_CONTAINER));

    when(enabledConfig.get(DEFAULT_CONTAINER, LOCKED_DOMAIN_SUFFIX_KEY)).thenReturn("-a.example.com:8080");
    when(enabledConfig.getContainers()).thenReturn(singletonList(DEFAULT_CONTAINER));
  }

  @Test
  public void testDisabledGlobally() {
    lockedDomainService = new HashLockedDomainService(requiredConfig, false);
    assertTrue(lockedDomainService.isSafeForOpenProxy("anywhere.com"));
    assertTrue(lockedDomainService.isSafeForOpenProxy("embed.com"));
    assertTrue(lockedDomainService.gadgetCanRender("embed.com", wantsLocked, "default"));
    assertTrue(lockedDomainService.gadgetCanRender("embed.com", notLocked, "default"));

    lockedDomainService = new HashLockedDomainService(enabledConfig, false);
    assertTrue(lockedDomainService.isSafeForOpenProxy("anywhere.com"));
    assertTrue(lockedDomainService.isSafeForOpenProxy("embed.com"));
    assertTrue(lockedDomainService.gadgetCanRender("embed.com", wantsLocked, "default"));
    assertTrue(lockedDomainService.gadgetCanRender("embed.com", notLocked, "default"));
  }

  @Test
  public void testEnabledForGadget() {
    lockedDomainService = new HashLockedDomainService(enabledConfig, true);
    assertFalse(lockedDomainService.isSafeForOpenProxy("images-a.example.com:8080"));
    assertFalse(lockedDomainService.isSafeForOpenProxy("-a.example.com:8080"));
    assertTrue(lockedDomainService.isSafeForOpenProxy("embed.com"));
    assertFalse(lockedDomainService.gadgetCanRender("www.example.com", wantsLocked, "default"));
    assertTrue(lockedDomainService.gadgetCanRender(
        "8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", wantsLocked, "default"));
    String target = lockedDomainService.getLockedDomainForGadget(wantsLocked, "default");
    assertEquals("8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", target);
  }

  @Test
  public void testNotEnabledForGadget() {
    lockedDomainService = new HashLockedDomainService(enabledConfig, true);

    assertFalse(lockedDomainService.isSafeForOpenProxy("images-a.example.com:8080"));
    assertFalse(lockedDomainService.isSafeForOpenProxy("-a.example.com:8080"));
    assertTrue(lockedDomainService.isSafeForOpenProxy("embed.com"));

    assertTrue(lockedDomainService.gadgetCanRender("www.example.com", notLocked, "default"));
    assertFalse(lockedDomainService.gadgetCanRender(
        "8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", notLocked, "default"));
    assertTrue(lockedDomainService.gadgetCanRender(
        "auvn86n7q0l4ju2tq5cq8akotcjlda66-a.example.com:8080", notLocked, "default"));
    assertNull(lockedDomainService.getLockedDomainForGadget(notLocked, "default"));
  }

  @Test
  public void testRequiredForContainer() {
    lockedDomainService = new HashLockedDomainService(requiredConfig, true);

    assertFalse(lockedDomainService.isSafeForOpenProxy("images-a.example.com:8080"));
    assertFalse(lockedDomainService.isSafeForOpenProxy("-a.example.com:8080"));
    assertTrue(lockedDomainService.isSafeForOpenProxy("embed.com"));

    assertFalse(lockedDomainService.gadgetCanRender("www.example.com", wantsLocked, "default"));
    assertFalse(lockedDomainService.gadgetCanRender("www.example.com", notLocked, "default"));

    String target = lockedDomainService.getLockedDomainForGadget(wantsLocked, "default");
    assertEquals("8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", target);
    target = lockedDomainService.getLockedDomainForGadget(notLocked, "default");
    assertEquals("auvn86n7q0l4ju2tq5cq8akotcjlda66-a.example.com:8080", target);

    assertTrue(lockedDomainService.gadgetCanRender(
        "8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", wantsLocked, "default"));
    assertFalse(lockedDomainService.gadgetCanRender(
        "auvn86n7q0l4ju2tq5cq8akotcjlda66-a.example.com:8080", wantsLocked, "default"));
    assertTrue(lockedDomainService.gadgetCanRender(
        "auvn86n7q0l4ju2tq5cq8akotcjlda66-a.example.com:8080", notLocked, "default"));
    assertFalse(lockedDomainService.gadgetCanRender(
        "8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", notLocked, "default"));
  }

  @Test
  public void testMissingConfig() {
    ContainerConfig containerMissingConfig = mock(ContainerConfig.class);
    when(containerMissingConfig.getContainers()).thenReturn(singletonList(DEFAULT_CONTAINER));

    lockedDomainService = new HashLockedDomainService(containerMissingConfig, true);

    assertFalse(lockedDomainService.gadgetCanRender("www.example.com", wantsLocked, "default"));
    assertTrue(lockedDomainService.gadgetCanRender("www.example.com", notLocked, "default"));
  }

  @Test
  public void testMultiContainer() {
    ContainerConfig inheritsConfig  = mock(ContainerConfig.class);
    when(inheritsConfig.getContainers()).thenReturn(Arrays.asList(DEFAULT_CONTAINER, "other"));
    when(inheritsConfig.get(isA(String.class), eq(LOCKED_DOMAIN_REQUIRED_KEY))).thenReturn("true");
    when(inheritsConfig.get(isA(String.class), eq(LOCKED_DOMAIN_SUFFIX_KEY))).thenReturn("-a.example.com:8080");

    lockedDomainService = new HashLockedDomainService(inheritsConfig, true);

    assertFalse(lockedDomainService.gadgetCanRender("www.example.com", wantsLocked, "other"));
    assertFalse(lockedDomainService.gadgetCanRender("www.example.com", notLocked, "other"));
    assertTrue(lockedDomainService.gadgetCanRender(
        "8uhr00296d2o3sfhqilj387krjmgjv3v-a.example.com:8080", wantsLocked, "other"));
  }
}
