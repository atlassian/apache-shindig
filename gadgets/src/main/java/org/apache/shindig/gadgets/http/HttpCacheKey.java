/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package org.apache.shindig.gadgets.http;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.gadgets.AuthType;
import org.json.JSONArray;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.json.JSONObject;

/**
 * Key for use in an HttpCache
 *
 * The key is made up of name/value pairs.  By default only the HTTP method
 * and URL are included in the cache key.  Use the set() method to add
 * additional data to the cache key.
 */
public class HttpCacheKey {

  private Map<String, String> data;
  private boolean cacheable;

  /**
   * Create a cache key for the specified request.
   *
   * @param request request param
   */
  public HttpCacheKey(HttpRequest request) {
    data = Maps.newHashMap();
    setCacheable(true);
    if (!"GET".equals(request.getMethod()) ||
        request.getIgnoreCache() ||
        request.getHeader("Authorization") != null) {
      setCacheable(false);
    }
    // In theory we only cache GET, but including the method in the cache key
    // provides some additional insurance that we aren't mixing cache content.
    set("method", request.getMethod());
    set("url", request.getUri().toString());
    set("authType", request.getAuthType().toString());
    set("ownerId", getOwnerId(request));
    set("viewerId", getViewerId(request));
    set("tokenOwner", getTokenOwner(request));
    set("appUrl", getAppUrl(request));
    set("instanceId", getInstanceId(request));
    set("serviceName", getServiceName(request));
    set("tokenName", getTokenName(request));
  }

  /**
   * Add additional data to the cache key.
   */
  public void set(String key, String value) {
    data.put(key, value);
  }

  /**
   * Remove data from the cache key.
   */
  public void remove(String key) {
    data.remove(key);
  }

  public void setCacheable(boolean cacheable) {
    this.cacheable = cacheable;
  }

  public boolean isCacheable() {
    return cacheable;
  }

  /**
   * Figure out a string representation of this cache key.  The representation
   * will be:
   *
   * canonical: identical sets of key/value pairs will always map to the same
   * string.
   *
   * unique: different sets of key/value pairs will always map to different
   * strings.
   */
  @Override
  public String toString() {
    List<String> list = Lists.newArrayList();
    list.addAll(data.keySet());
    Collections.sort(list);
    JSONArray json = new JSONArray();
    for (String key : list) {
      if (data.get(key) == null) {
        json.put(Collections.singletonMap(key, JSONObject.NULL));
      }else{
        json.put(Collections.singletonMap(key, data.get(key)));
      }
    }
    return json.toString();
  }

  protected static String getOwnerId(HttpRequest request) {
    if (request.getAuthType() != AuthType.NONE &&
        request.getOAuthArguments().getSignOwner()) {
      String ownerId = request.getSecurityToken().getOwnerId();
      return ownerId == null ? "" : ownerId;
    }
    // Requests that don't use authentication can share the result.
    return null;
  }

  protected static String getViewerId(HttpRequest request) {
    if (request.getAuthType() != AuthType.NONE &&
        request.getOAuthArguments().getSignViewer()) {
      String viewerId = request.getSecurityToken().getViewerId();
      return viewerId == null ? "" : viewerId;
    }
    // Requests that don't use authentication can share the result.
    return null;
  }

  protected static String getTokenOwner(HttpRequest request) {
    SecurityToken st = request.getSecurityToken();
    if (request.getAuthType() != AuthType.NONE &&
        st.getOwnerId() != null
        && st.getOwnerId().equals(st.getViewerId())
        && request.getOAuthArguments().mayUseToken()) {
      return st.getOwnerId();
    }
    // Requests that don't use authentication can share the result.
    return null;
  }

  protected static String getAppUrl(HttpRequest request) {
    if (request.getAuthType() != AuthType.NONE) {
      return request.getSecurityToken().getAppUrl();
    }
    // Requests that don't use authentication can share the result.
    return null;
  }

  protected static String getInstanceId(HttpRequest request) {
    if (request.getAuthType() != AuthType.NONE) {
      return request.getSecurityToken().getModuleId();
    }
    // Requests that don't use authentication can share the result.
    return null;
  }

  protected static String getServiceName(HttpRequest request) {
    if (request.getAuthType() != AuthType.NONE) {
      return request.getOAuthArguments().getServiceName();
    }
    // Requests that don't use authentication can share the result.
    return null;
  }

  protected static String getTokenName(HttpRequest request) {
    if (request.getAuthType() != AuthType.NONE) {
      return request.getOAuthArguments().getTokenName();
    }
    // Requests that don't use authentication can share the result.
    return null;
  }
}
