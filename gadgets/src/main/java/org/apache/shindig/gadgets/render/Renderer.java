package org.apache.shindig.gadgets.render;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.shindig.common.ContainerConfig;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.Gadget;
import org.apache.shindig.gadgets.GadgetContext;
import org.apache.shindig.gadgets.GadgetException;
import org.apache.shindig.gadgets.LockedDomainService;
import org.apache.shindig.gadgets.process.ProcessingException;
import org.apache.shindig.gadgets.process.Processor;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.apache.shindig.gadgets.spec.View;

import com.google.inject.Inject;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.regex.Pattern;

/**
 * Validates a rendering request parameters before calling an appropriate renderer.
 */
public class Renderer {

  private static final Logger LOG = LoggerFactory.getLogger(Renderer.class.getName());

  private final Processor processor;
  private final HtmlRenderer renderer;
  private final ContainerConfig containerConfig;
  private final LockedDomainService lockedDomainService;

  @Inject
  public Renderer(Processor processor,
                  HtmlRenderer renderer,
                  ContainerConfig containerConfig,
                  LockedDomainService lockedDomainService) {
    this.processor = processor;
    this.renderer = renderer;
    this.containerConfig = containerConfig;
    this.lockedDomainService = lockedDomainService;
  }

  /**
   * Attempts to render the requested gadget.
   *
   * @return The results of the rendering attempt.
   *
   * TODO: Localize error messages.
   */
  public RenderingResults render(GadgetContext context) {
    if (!validateParent(context)) {
      return RenderingResults.error("Unsupported parent parameter. Check your container code.");
    }

    try {
      Gadget gadget = processor.process(context);

      if (gadget.getCurrentView() == null) {
        return RenderingResults.error("Unable to locate an appropriate view in this gadget. " +
            "Requested: '" + StringEscapeUtils.escapeHtml4(gadget.getContext().getView()) +
            "' Available: " + StringEscapeUtils.escapeHtml4(gadget.getSpec().getViews().keySet().toString()));
      }

      if (gadget.getCurrentView().getType() == View.ContentType.URL) {
        return RenderingResults.mustRedirect(getRedirect(gadget));
      }

      GadgetSpec spec = gadget.getSpec();
      if (!lockedDomainService.gadgetCanRender(context.getHost(), spec, context.getContainer())) {
        return RenderingResults.mustRedirect(getRedirect(gadget));
      }

      return RenderingResults.ok(renderer.render(gadget));
    } catch (RenderingException | ProcessingException e) {
      return logError(context.getUrl(), e);
    } catch (RuntimeException e) {
      if (e.getCause() instanceof GadgetException) {
        return logError(context.getUrl(), e.getCause());
      }
      throw e;
    }
  }

  private RenderingResults logError(URI gadgetUrl, Throwable t) {
    LOG.info("Failed to render gadget " + gadgetUrl + ": " + t.getMessage());
    return RenderingResults.error(t.getMessage());
  }

  /**
   * Validates that the parent parameter was acceptable.
   *
   * @return True if the parent parameter is valid for the current container.
   */
  private boolean validateParent(GadgetContext context) {
    String container = context.getContainer();
    String parent = context.getParameter("parent");

    if (parent == null) {
      // If there is no parent parameter, we are still safe because no
      // dependent code ever has to trust it anyway.
      return true;
    }

    try {
      JSONArray parents = containerConfig.getJsonArray(container, "gadgets.parent");
      if (parents == null) {
        return true;
      }
      // We need to check each possible parent parameter against this regex.
      for (int i = 0, j = parents.length(); i < j; ++i) {
        if (Pattern.matches(parents.get(i).toString(), parent)) {
          return true;
        }
      }
    } catch (JSONException e) {
      LOG.warn("Configuration error", e);
    }
    return false;
  }

  private Uri getRedirect(Gadget gadget) {
    // TODO: This should probably just call UrlGenerator.getIframeUrl(), but really it should
    // never happen.
    View view = gadget.getCurrentView();
    if (view.getType() == View.ContentType.URL) {
      return gadget.getCurrentView().getHref();
    }
    // TODO
    return null;
  }
}
