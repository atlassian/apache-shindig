/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.gadgets.preload;

import com.google.inject.Inject;
import org.apache.shindig.gadgets.GadgetContext;
import org.apache.shindig.gadgets.GadgetException;
import org.apache.shindig.gadgets.spec.GadgetSpec;

import java.util.List;

/**
 * Preloads will be fetched synchronously.
 */
public class DefaultPreloaderService implements PreloaderService {
  private final List<? extends Preloader> preloaders;

  @Inject
  public DefaultPreloaderService(List<Preloader> preloaders) {
    this.preloaders = preloaders;
  }

  public Preloads preload(GadgetContext context, GadgetSpec gadget) throws GadgetException {
    DefaultPreloads preloads = new DefaultPreloads();

    for (Preloader preloader : preloaders) {
      preloads.putAll(preloader.createPreloadDataMap(context, gadget));
    }

    return preloads;
  }
}
