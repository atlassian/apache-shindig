/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets;

import org.apache.shindig.common.cache.Cache;
import org.apache.shindig.common.cache.CacheProvider;
import org.apache.shindig.common.cache.SoftExpiringCache;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.common.util.Check;
import org.apache.shindig.common.util.HashUtil;
import org.apache.shindig.gadgets.http.HttpFetcher;
import org.apache.shindig.gadgets.http.HttpRequest;
import org.apache.shindig.gadgets.http.HttpResponse;
import org.apache.shindig.gadgets.spec.GadgetSpec;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of a gadget spec factory.
 */
@Singleton
public class DefaultGadgetSpecFactory implements GadgetSpecFactory {
  public static final String CACHE_NAME = "gadgetSpecs";
  static final String RAW_GADGETSPEC_XML_PARAM_NAME = "rawxml";
  static final Uri RAW_GADGET_URI = Uri.parse("http://localhost/raw.xml");
  static final String ERROR_SPEC = "<Module><ModulePrefs title='Error'/><Content/></Module>";
  static final String ERROR_KEY = "parse.exception";
  static final Logger LOG = LoggerFactory.getLogger(DefaultGadgetSpecFactory.class.getName());

  private final HttpFetcher fetcher;
  private final SoftExpiringCache<Uri, GadgetSpec> cache;
  private final long refresh;

  /**
   * locksByHosts keeps one lock per host. It prevents updating gadgets spec by two or more threads at the same time
   */
  private final Map<String, ReentrantLock> locksByHosts = new ConcurrentHashMap<>();

  @Inject
  public DefaultGadgetSpecFactory(HttpFetcher fetcher,
                                  CacheProvider cacheProvider,
                                  @Named("shindig.cache.xml.refreshInterval") long refresh) {
    this.fetcher = fetcher;
    Cache<Uri, GadgetSpec> baseCache = cacheProvider.createCache(CACHE_NAME);
    this.cache = new SoftExpiringCache<Uri, GadgetSpec>(baseCache);
    this.refresh = refresh;
  }

  public GadgetSpec getGadgetSpec(GadgetContext context) throws GadgetException {
    String rawxml = context.getParameter(RAW_GADGETSPEC_XML_PARAM_NAME);
    if (rawxml != null) {
      // Set URI to a fixed, safe value (localhost), preventing a gadget rendered
      // via raw XML (eg. via POST) to be rendered on a locked domain of any other
      // gadget whose spec is hosted non-locally.
      return new GadgetSpec(RAW_GADGET_URI, rawxml);
    }
    return getGadgetSpec(context.getUrl(), context.getIgnoreCache());
  }

  /**
   * Retrieves a gadget specification from the cache or from the Internet.
   */
  public GadgetSpec getGadgetSpec(URI gadgetUri, boolean ignoreCache) throws GadgetException {
    Uri uri = Uri.fromJavaUri(gadgetUri);
    if (ignoreCache) {
      return fetchObjectAndCache(uri, ignoreCache);
    }

    final SoftExpiringCache.CachedObject<GadgetSpec> cached = cache.getElement(uri);

    // The best case: the required object is cached, and it is not expired
    // No other actions are required. Just return it
    if (cached != null && !cached.isExpired) {
      return getGadgetSpecOrThrowGadgetException(cached.obj);
    }

    // The required object is still exists, but it is expired
    // We should invalidate this object in the cache only if no other threads are updating gadget spec
    // at the same time for the same host
    // We have to do it for performance reasons: for example, if a gadget spec provider is
    // extremely slow, we will allow one and only one thread waiting for an answer
    // Meanwhile, all other threads will use an expired value
    if (cached != null) {
      final String host = gadgetUri.getHost();
      final ReentrantLock lock = locksByHosts.computeIfAbsent(host, s -> new ReentrantLock());
      if (!lock.tryLock()) {
        // something else is updating gadget spec at this time, so we just return what we have
        return getGadgetSpecOrThrowGadgetException(cached.obj);
      }

      try {
        return getGadgetSpecOrThrowGadgetException(fetchObjectAndCache(uri, ignoreCache));
      } catch (GadgetException e) {
        return addGadgetExceptionToCache(uri, e);
      } finally {
        lock.unlock();
      }
    }

    // Usually, this case happens in the beginning (no records are in cache)
    // When we do not have anything in cache, we allow all threads to send requests
    // We can't change this logic because other libraries can rely on it
    try {
      return getGadgetSpecOrThrowGadgetException(fetchObjectAndCache(uri, ignoreCache));
    } catch (GadgetException e) {
      return addGadgetExceptionToCache(uri, e);
    }
  }

  private GadgetSpec addGadgetExceptionToCache(Uri uri, GadgetException e) throws GadgetException {
    // We create this dummy spec to avoid the cost of re-parsing when a remote site is out.
    final GadgetSpec spec = new GadgetSpec(uri, ERROR_SPEC);
    spec.setAttribute(ERROR_KEY, e);
    LOG.info("GadgetSpec fetch failed for {}. Results are put to the cache", uri);
    cache.addElement(uri, spec, refresh);
    return getGadgetSpecOrThrowGadgetException(spec);
  }

  private GadgetSpec getGadgetSpecOrThrowGadgetException(GadgetSpec gadgetSpec) throws GadgetException {
    GadgetException exception = (GadgetException) gadgetSpec.getAttribute(ERROR_KEY);
    if (exception != null) {
      throw exception;
    }
    return gadgetSpec;
  }

  /**
   * Retrieves a gadget specification from the Internet, processes its views and
   * adds it to the cache.
   */
  private GadgetSpec fetchObjectAndCache(Uri url, boolean ignoreCache) throws GadgetException {
    HttpRequest request = new HttpRequest(url).setIgnoreCache(ignoreCache);
    // Since we don't allow any variance in cache time, we should just force the cache time
    // globally. This ensures propagation to shared caches when this is set.
    request.setCacheTtl((int) (refresh / 1000));

    HttpResponse response = fetcher.fetch(request);
    if (response.getHttpStatusCode() != HttpResponse.SC_OK) {
      throw new GadgetException(GadgetException.Code.FAILED_TO_RETRIEVE_CONTENT,
                                "Unable to retrieve gadget xml. HTTP error " +
                                response.getHttpStatusCode());
    }

    // This might not be good enough; should we take message bundle changes into account?
    String checksum = HashUtil.checksum(response.getResponseAsBytes());

    GadgetSpec spec = new GadgetSpec(url, response.getResponseAsString(), checksum);
    cache.addElement(url, spec, refresh);
    return spec;
  }
}
