package org.apache.shindig.social.sample.service;

import com.google.inject.Inject;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.social.ResponseError;
import org.apache.shindig.social.core.util.ContainerConf;
import org.apache.shindig.social.opensocial.service.DataRequestHandler;
import org.apache.shindig.social.opensocial.service.RequestItem;
import org.apache.shindig.social.opensocial.spi.SocialSpiException;
import org.apache.shindig.social.sample.spi.JsonDbOpensocialService;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.Future;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.nio.charset.StandardCharsets.UTF_8;

public class SampleContainerHandler extends DataRequestHandler {

  private final JsonDbOpensocialService service;

  private static final String POST_PATH = "/samplecontainer/{type}/{doevil}";

  @Inject
  public SampleContainerHandler(JsonDbOpensocialService dbService, ContainerConf containerConf) {
    super(containerConf);
    this.service = dbService;
  }

  /**
   * We don't support any delete methods right now.
   */
  protected Future<?> handleDelete(RequestItem request) throws SocialSpiException {
    throw new SocialSpiException(ResponseError.NOT_IMPLEMENTED, null);
  }

  /**
   * We don't distinguish between put and post for these urls.
   */
  protected Future<?> handlePut(RequestItem request) throws SocialSpiException {
    return handlePost(request);
  }

  /**
   * Handles /samplecontainer/setstate and /samplecontainer/setevilness/{doevil}. TODO(doll): These
   * urls aren't very resty. Consider changing the samplecontainer.html calls post.
   */
  protected Future<?> handlePost(RequestItem request) throws SocialSpiException {
    request.applyUrlTemplate(POST_PATH);
    String type = request.getParameter("type");
    if (type.equals("setstate")) {
      try {
        String stateFile = request.getParameter("fileurl");
        service.setDb(new JSONObject(fetchStateDocument(stateFile)));
      } catch (JSONException e) {
        throw new SocialSpiException(ResponseError.BAD_REQUEST,
            "The json state file was not valid json", e);
      }
    } else if (type.equals("setevilness")) {
      throw new SocialSpiException(ResponseError.NOT_IMPLEMENTED,
          "evil data has not been implemented yet");
    }

    return ImmediateFuture.newInstance(null);
  }

  /**
   * Handles /samplecontainer/dumpstate
   */
  protected Future<?> handleGet(RequestItem request) {
    return ImmediateFuture.newInstance(service.getDb());
  }

  private String fetchStateDocument(final String stateFileLocation) {
    final HttpGet httpGet = new HttpGet(stateFileLocation);
    try (final CloseableHttpClient httpclient = HttpClients.createDefault();
         final CloseableHttpResponse response = httpclient.execute(httpGet)) {
      if (response.getStatusLine().getStatusCode() != HTTP_OK) {
        throw new RuntimeException("The json state file " + stateFileLocation + " could not be fetched and parsed.");
      }
      return EntityUtils.toString(response.getEntity(), UTF_8);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
