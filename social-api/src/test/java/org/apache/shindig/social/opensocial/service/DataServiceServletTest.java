/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.social.opensocial.service;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import org.apache.shindig.common.testing.FakeGadgetToken;
import org.apache.shindig.common.testing.FakeHttpServletRequest;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.social.ResponseError;
import org.apache.shindig.social.SocialApiTestsGuiceModule;
import org.apache.shindig.social.core.util.BeanJsonConverter;
import org.apache.shindig.social.core.util.BeanXStreamAtomConverter;
import org.apache.shindig.social.core.util.BeanXStreamConverter;
import org.apache.shindig.social.core.util.xstream.XStream081Configuration;
import org.apache.shindig.social.opensocial.spi.SocialSpiException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.apache.shindig.social.opensocial.service.RestfulRequestItem.X_HTTP_METHOD_OVERRIDE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DataServiceServletTest {

  private static final FakeGadgetToken FAKE_GADGET_TOKEN = new FakeGadgetToken()
      .setOwnerId("john.doe").setViewerId("john.doe");

  private HttpServletRequest req;
  private HttpServletResponse res;
  private DataServiceServlet servlet;
  private PersonHandler peopleHandler;
  private ActivityHandler activityHandler;
  private AppDataHandler appDataHandler;
  private BeanJsonConverter jsonConverter;

  private final ServletInputStream dummyPostData = new ServletInputStream() {
    @Override public int read()  {
      return -1;
    }
  };

  @Before
  public void setUp() throws Exception {
    servlet = new DataServiceServlet();
    req = mock(HttpServletRequest.class);
    res = mock(HttpServletResponse.class);
    jsonConverter = mock(BeanJsonConverter.class);
    BeanXStreamConverter xmlConverter = mock(BeanXStreamConverter.class);
    BeanXStreamAtomConverter atomConverter = mock(BeanXStreamAtomConverter.class);
    peopleHandler = mock(PersonHandler.class);
    activityHandler = mock(ActivityHandler.class);
    appDataHandler = mock(AppDataHandler.class);

    when(jsonConverter.getContentType()).thenReturn("application/json");
    when(xmlConverter.getContentType()).thenReturn("application/xml");
    when(atomConverter.getContentType()).thenReturn("application/atom+xml");

    HandlerDispatcher dispatcher = new StandardHandlerDispatcher(constant(peopleHandler),
        constant(activityHandler), constant(appDataHandler));
    servlet.setHandlerDispatcher(dispatcher);

    servlet.setBeanConverters(jsonConverter, xmlConverter, atomConverter);
  }

  // TODO: replace with Providers.of() when Guice version is upgraded
  private static <T> Provider<T> constant(final T value) {
    return () -> value;
  }

  @Test
  public void testPeopleUriRecognition() throws Exception {
    verifyHandlerWasFoundForPathInfo('/'
        + DataServiceServlet.PEOPLE_ROUTE + "/5/@self", peopleHandler);
  }

  @Test
  public void testActivitiesUriRecognition() throws Exception {
    verifyHandlerWasFoundForPathInfo('/'
        + DataServiceServlet.ACTIVITY_ROUTE + "/5/@self", activityHandler);
  }

  @Test
  public void testAppDataUriRecognition() throws Exception {
    verifyHandlerWasFoundForPathInfo('/'
        + DataServiceServlet.APPDATA_ROUTE + "/5/@self", appDataHandler);
  }

  @Test
  public void testMethodOverride() throws Exception {
    String route = '/' + DataServiceServlet.APPDATA_ROUTE;
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "POST", "GET");
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "POST", "");
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "POST", null);
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "POST", "POST");
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "GET", null);
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "DELETE", null);
    verifyHandlerWasFoundForPathInfo(route, appDataHandler, "PUT", null);
  }

  /**
   * Tests a data handler that returns a failed Future
   */
  @Test
  public void testFailedRequest() throws Exception {
    String route = '/' + DataServiceServlet.APPDATA_ROUTE;
    setupRequest(route, "GET", null);

    when(appDataHandler.handleItem(isA(RestfulRequestItem.class))).thenReturn(
        ImmediateFuture.errorInstance(new RuntimeException("FAILED")));

    res.sendError(500, "FAILED");
    res.setCharacterEncoding("UTF-8");
    res.setContentType("application/json");

    servlet.service(req, res);
  }

  private void verifyHandlerWasFoundForPathInfo(String peoplePathInfo, DataRequestHandler handler)
      throws Exception {
    String post = "POST";
    verifyHandlerWasFoundForPathInfo(peoplePathInfo, handler, post, post);
  }

  private void verifyHandlerWasFoundForPathInfo(String pathInfo, DataRequestHandler handler,
                                                String actualMethod, String overrideMethod) throws Exception {
    setupRequest(pathInfo, actualMethod, overrideMethod);

    String jsonObject = "my lovely json";

    when(handler.handleItem(isA(RequestItem.class)))
            .then((Answer<Future<?>>) invocationOnMock -> ImmediateFuture.newInstance(jsonObject));

    when(jsonConverter.convertToString(ImmutableMap.of("entry", jsonObject)))
            .thenReturn("{ 'entry' : " + jsonObject + " }");

    PrintWriter writerMock = mock(PrintWriter.class);
    when(res.getWriter()).thenReturn(writerMock);
    writerMock.write(jsonObject);
    res.setCharacterEncoding("UTF-8");
    res.setContentType("application/json");

    servlet.service(req, res);
    // ick, this resets for the next call...
    when(jsonConverter.getContentType()).thenReturn("application/json");
  }

  private void setupRequest(String pathInfo, String actualMethod, String overrideMethod)
      throws IOException {
    when(req.getCharacterEncoding()).thenReturn("UTF-8");
    if (!("GET").equals(overrideMethod) && !("HEAD").equals(overrideMethod)) {
      when(req.getInputStream()).thenReturn(dummyPostData);
    }
    when(req.getPathInfo()).thenReturn(pathInfo);
    when(req.getMethod()).thenReturn(actualMethod);
    when(req.getParameterNames()).thenReturn(new StringTokenizer(""));
    when(req.getParameter(X_HTTP_METHOD_OVERRIDE)).thenReturn(overrideMethod);
    when(req.getParameter(DataServiceServlet.FORMAT_PARAM)).thenReturn(null);

    when(req.getAttribute(isA(String.class))).thenReturn(FAKE_GADGET_TOKEN);
  }

  @Test
  public void testInvalidRoute() throws Exception {
    RestfulRequestItem requestItem = new RestfulRequestItem("/ahhh!", "GET", null,
        FAKE_GADGET_TOKEN, jsonConverter);
    try {
      servlet.handleRequestItem(requestItem, new FakeHttpServletRequest()).get();
      fail();
    } catch (ExecutionException ee) {
      assertTrue(ee.getCause() instanceof SocialSpiException);
      assertEquals(ResponseError.NOT_IMPLEMENTED, ((SocialSpiException) ee.getCause()).getError());
    }
  }

  @Test
  public void testGetConverterForRequest() {

    Injector injector = Guice.createInjector(new SocialApiTestsGuiceModule());
    BeanJsonConverter json = new BeanJsonConverter(injector);
    BeanXStreamConverter xml = new BeanXStreamConverter(new XStream081Configuration(injector));
    BeanXStreamAtomConverter atom = new BeanXStreamAtomConverter(new XStream081Configuration(injector));
    servlet.setBeanConverters(json, xml, atom);

    assertConverter(atom, "atom");
    assertConverter(xml, "xml");
    assertConverter(json, "");
    assertConverter(json, null);
    assertConverter(json, "ahhhh!");
  }

  @Test
  public void testGetConverterForRequestContentType() {
    Injector injector = Guice.createInjector(new SocialApiTestsGuiceModule());
    BeanJsonConverter json = new BeanJsonConverter(injector);
    BeanXStreamConverter xml = new BeanXStreamConverter(new XStream081Configuration(injector));
    BeanXStreamAtomConverter atom = new BeanXStreamAtomConverter(new XStream081Configuration(injector));
    servlet.setBeanConverters(json, xml, atom);

    assertConverterForContentType(atom, "application/atom+xml");
    assertConverterForContentType(xml, "application/xml");
    assertConverterForContentType(json, "");
    assertConverterForContentType(json, null);
    assertConverterForContentType(json, "abcd!");

  }

  private void assertConverter(BeanConverter converter, String format) {
    when(req.getParameter(DataServiceServlet.FORMAT_PARAM))
        .thenReturn(format);
    assertEquals(converter, servlet.getConverterForRequest(req));
  }

  private void assertConverterForContentType(BeanConverter converter,
      String contentType) {
    when(req.getHeader(DataServiceServlet.CONTENT_TYPE)).thenReturn(contentType);
    assertEquals(converter, servlet.getConverterForRequest(req));
  }
}
