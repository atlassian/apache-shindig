/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.apache.shindig.social.opensocial.service;

import com.google.common.collect.Lists;
import com.google.inject.Guice;
import org.apache.shindig.common.testing.FakeGadgetToken;
import org.apache.shindig.social.core.util.BeanJsonConverter;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.PersonService;
import org.apache.shindig.social.opensocial.spi.UserId;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RestfulRequestItemTest {

  private static final FakeGadgetToken FAKE_TOKEN = new FakeGadgetToken();

  private static final String DEFAULT_PATH = "/people/john.doe/@self";

  private RestfulRequestItem request;

  @Before
  public void setUp() throws Exception {
    request = new RestfulRequestItem(
        DEFAULT_PATH + "?fields=huey,dewey,louie", "GET",
        "{name: 'Bob', id: '1234'}", FAKE_TOKEN, new BeanJsonConverter(Guice.createInjector()));
  }

  @Test
  public void testParseUrl() {
    assertEquals("people", request.getService());
    assertEquals(Lists.newArrayList("huey", "dewey", "louie"), request.getListParameter("fields"));

    // Try it without any params
    request = new RestfulRequestItem(DEFAULT_PATH, "GET", null, null, null);

    assertEquals("people", request.getService());
    assertNull(request.getParameters().get("fields"));
  }

  @Test
  public void testGetHttpMethodFromParameter() {
    HttpServletRequest overridden = mock(HttpServletRequest.class);
    when(overridden.getParameter(RestfulRequestItem.X_HTTP_METHOD_OVERRIDE)).thenReturn("DELETE");
    assertEquals("DELETE", RestfulRequestItem.getMethod(overridden));
  }


  @Test
  public void testGetAppId() {
    request.setParameter("appId", "100");
    assertEquals("100", request.getAppId());

    request.setParameter("appId", "@app");
    assertEquals(FAKE_TOKEN.getAppId(), request.getAppId());
  }

  @Test
  public void testGetUser() {
    request.setParameter("userId", "@owner");
    assertEquals(UserId.Type.owner, request.getUsers().iterator().next().getType());
  }

  @Test
  public void testGetGroup() {
    request.setParameter("groupId", "@self");
    assertEquals(GroupId.Type.self, request.getGroup().getType());
  }

  @Test
  public void testStartIndex() {
    request.setParameter("startIndex", null);
    assertEquals(0, request.getStartIndex());

    request.setParameter("startIndex", "5");
    assertEquals(5, request.getStartIndex());
  }

  @Test
  public void testCount() {
    request.setParameter("count", null);
    assertEquals(20, request.getCount());

    request.setParameter("count", "5");
    assertEquals(5, request.getCount());
  }

  @Test
  public void testSortOrder() {
    request.setParameter("sortOrder", null);
    assertEquals(PersonService.SortOrder.ascending, request.getSortOrder());

    request.setParameter("sortOrder", "descending");
    assertEquals(PersonService.SortOrder.descending, request.getSortOrder());
  }

  @Test
  public void testFields() {
    request.setListParameter("fields", emptyList());
    assertEquals(newHashSet(), request.getFields());

    request.setParameter("fields", "happy,sad,grumpy");
    assertEquals(newHashSet("happy", "sad", "grumpy"), request.getFields());
  }

  @Test
  public void testRouteFromParameter() {
    assertEquals("path", RestfulRequestItem.getServiceFromPath("/path"));
    assertEquals("path", RestfulRequestItem.getServiceFromPath("/path/fun"));
    assertEquals("path", RestfulRequestItem.getServiceFromPath("/path/fun/yes"));
  }

  public static class InputData {
    String name;
    int id;

    public void setName(String name) {
      this.name = name;
    }

    public void setId(int id) {
      this.id = id;
    }
  }

  @Test
  public void testGetTypedParameter() {
    InputData input = request.getTypedParameter("anykey", InputData.class);
    assertEquals("Bob", input.name);
    assertEquals(1234, input.id);
  }

  @Test
  public void testGetTypedParameters() {
    InputData input = request.getTypedParameters(InputData.class);
    assertEquals("Bob", input.name);
    assertEquals(1234, input.id);
  }
}
